# -*- coding: utf-8 -*-
{
    'name': "Phone, Mobile & Email Searching",
    'author': "saas tools",
    'price': 9.90,
    'currency': 'EUR',
    'website': 'https://www.saastools.xyz',
    'summary': 'Searching mobile, phone & email made by (W360S)',
    'description': 'This module add more feature for searching by phone, email & mobile number help you easy to search anything related to contact make more easy to find your contact exsit in your odoo system',
    'category': 'Sales',
    'license': 'LGPL-3',
    'version': '1.0.0',
    'depends': ['base'],
    'data': [
    ],
    "installable": True,
    "images":['static/description/banner.jpg'],
}